from django import forms
from .models import Ponudba, Karta
from dal import autocomplete


class EditPonudbaForm(forms.Form):
    cena = forms.FloatField()
    dodatno = forms.CharField(max_length=200, required=False)

class PonudbaForm(forms.Form):
    tip = forms.ChoiceField(choices= Ponudba.TIPI)
    cena = forms.FloatField()
    dodatno = forms.CharField(max_length=200, required=False)
    karta = forms.ModelChoiceField(queryset=Karta.objects.all(),widget=autocomplete.ModelSelect2(url='ponudbe:card_autocomplete'))


class MnenjeForm(forms.Form):
    mnenje = forms.CharField(required=False, widget=forms.Textarea(
        attrs={
            "rows": 4,
            "class": "h-100 w-100"
        }
    ) )