from django import forms
from dal import autocomplete

from main.models import ReportType, Ticket

class MarkForm(forms.Form):
    description = forms.CharField(widget=forms.Textarea(
        attrs={
            "rows": 12,
            "class": "h-100 w-100"
        }
    ) )

class ReplyForm(forms.Form):
    content = forms.CharField(widget=forms.Textarea(
        attrs={
            "rows": 4,
            "class": "h-100 w-100"
        }
    ) )


class ReportForm(forms.Form):
    report_type = forms.ModelMultipleChoiceField(queryset=ReportType.objects.all(),widget=forms.CheckboxSelectMultiple)
    more_information = forms.CharField(
        required=True,
        widget=forms.Textarea(
        attrs={
            "rows": 4,
            "class": "h-100 w-100"
        }
    ) )