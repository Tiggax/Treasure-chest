from django.db import models
from django.conf import settings
from django.db.models import CharField, Model, IntegerField, ManyToManyField, FloatField, ForeignKey, DateField, BooleanField

# Create your models here.

from main.models import Komentar, Uporabnik


class Set(Model):
    OP = "OP", "One Piece"
    EB = "EB", "Extra Booster"
    PR = "PR", "Promotional"
    tips = [OP, EB, PR]

    ime = CharField(max_length=50)
    zapored = IntegerField()
    tip = CharField(max_length=2, choices=tips)

    def code(self):
        return f"{self.tip}{self.zapored:02}"
    

    def __str__(self):
        code = self.code()
        return f"{code}: {self.ime}"
    
    

class Barva(Model):
    barva = CharField(max_length=6)
    short = CharField(max_length=2)

    def __str__(self):
        return self.barva
    
class Skupina(Model):
    ime = CharField(max_length=50)

    def __str__(self):
        return f"{self.ime}"


class Karta(Model):

    RARITY_CHOICES = [
        (
            "normal",
            (
                ("C", "Common"),
                ("UC", "Uncommon"),
                ("R", "Rare"),
                ("SR", "Super Rare"),
                ("SEC", "Secret"),
            ),
        ),
        (
            "alternate",
            (
                ("Rp", "Rare+"),
                ("SRp", "Super Rare+"),
                ("SEA", "Secret+"),
                ("M", "Manga")
            )
        )
    ]
    VRSTA_CHOICES = [
        ("L", "Leader"),
        ("C", "Character"),
        ("S", "Stage"),
        ("E", "Event"),
    ]
    TOCKE = [
        ("N", "None"),
        ("1", 1),
        ("2", 2),
        ("3", 3),
        ("4", 4),
        ("5", 5),
        ("6", 6),
    ]
    NAPADI = [
        ("SLH", "Slash"),
        ("STR", "Strength"),
        ("SPC", "Special"),
        ("WIS", "Wisdom"),
        ("RNG", "Ranged"),
    ]

    zaporedna = IntegerField()
    ime = CharField(max_length=50)
    barva = ManyToManyField(Barva)
    opis = CharField(max_length=255)
    redkost = CharField(max_length=3, choices=RARITY_CHOICES)
    vrsta = CharField(max_length=1, choices=VRSTA_CHOICES)
    tocke = CharField(max_length=1, choices= TOCKE, null= True, blank= True)
    napad = CharField(max_length=3, choices=NAPADI, null= True, blank= True)
    skoda = IntegerField(null= True, blank= True)
    stevec = IntegerField(null= True, blank= True)
    strosek = IntegerField(null= True, blank= True)
    verzija = IntegerField()
    vrednost = FloatField()
    skupine = ManyToManyField(Skupina)
    set_id = ForeignKey(Set, on_delete=models.CASCADE)


    def is_alt(self):
        return self.redkost in self.rarity_choices[1]
    
    def __str__(self) -> str:
        code = self.set_id.code()
        return f"{self.ime} ({code}-{self.zaporedna:03}) (V.{self.verzija})"


class Ponudba(Model):
    STANJA = [
        ("N", "Na voljo"),
        ("T", "v Transakciji"),
        ("G", "V ponudbi"),
        ("V", "v Prevzemu"),
        ("Z", "Zaključeno"),
        ("P", "Preklicano"),
    ]
    TIPI = [
        ("K", "Kupim"),
        ("P", "Prodam")
    ]
    stanje = CharField(max_length=1, choices=STANJA, default=STANJA[0][0])
    datum = DateField(auto_now_add=True)
    tip = CharField(max_length=1, choices=TIPI)
    cena = FloatField()
    dodatno = CharField(max_length=255)
    karta = ForeignKey(Karta, on_delete=models.CASCADE)
    ustvaril = ForeignKey(Uporabnik, on_delete=models.CASCADE, related_name="ust")
    zakljucil = ForeignKey(Uporabnik, on_delete=models.CASCADE, null= True, blank= True, related_name="zak")
    mnenje = ForeignKey(Komentar, on_delete=models.CASCADE, null=True, blank=True)