from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render, get_object_or_404, get_list_or_404

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth import REDIRECT_FIELD_NAME

from main.filters import UserFilter
from ponudbe.models import Ponudba

from .models import Superuser, Ticket, Uporabnik, Komentar
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.db.models import Count

from .forms import MarkForm, ReplyForm, ReportForm

import markdown

# Create your views here.

def index(req):
    ctx = {}
    if req.user.is_authenticated:
        upr = req.user.uporabnik
        ponudbe = Ponudba.objects.filter(ustvaril=upr).filter(tip="K")
        prodajajo = Ponudba.objects.filter(tip="P")
        ctx["range"] = []
        ctx["all"] = []

        in_range = {}
        other = {}
        for ponudba in ponudbe:
            in_range[ponudba.karta] = prodajajo.filter(karta=ponudba.karta).filter(cena__lt = ponudba.cena)
            other[ponudba.karta] = prodajajo.filter(karta=ponudba.karta).filter(cena__gt = ponudba.cena)
        ctx["range"] = in_range
        ctx["all"] = other
    
    return render(req, 'home.html', context=ctx)

class SignUpView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"

@login_required(login_url='/accounts/login/')
def profile(req):
    ctx = {}
    ctx["uporabnik"] = get_object_or_404(Uporabnik, user = req.user)
    if req.method == "POST":
        form = MarkForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            req.user.uporabnik.description = data["description"]
            req.user.uporabnik.save()

            return HttpResponseRedirect(req.META.get('HTTP_REFERER'))
    else:
        current = ctx["uporabnik"].description
        form = MarkForm(initial={
            "description": current
        })

    ctx["markform"] = form
    md = markdown.Markdown(extensions=["extra"])
    ctx["md"] = md.convert(ctx["uporabnik"].description)
    ctx["reply_form"] = ReplyForm()
    ctx["report_form"] = ReportForm()


    return render(req, 'registration/profile.html', context=ctx)

def search_user(req):
    ctx = {}
    users = Uporabnik.objects.all()
    ctx["filter"] = UserFilter(req.GET, queryset=users)
    
    return render(req, 'registration/users.html', context=ctx)

def get_user(req, username):
    ctx = {}
    ctx["reply_form"] = ReplyForm()
    ctx["report_form"] = ReportForm()
    user = get_object_or_404(User, username=username)
    ctx["uporabnik"] = get_object_or_404(Uporabnik, user=user)
    ponudbe = Ponudba.objects.filter(ustvaril=user.uporabnik).filter(stanje="N")
    ctx["kupim"] = ponudbe.filter(tip="K")
    ctx["prodam"] = ponudbe.filter(tip="P")
    md = markdown.Markdown(extensions=["extra"])
    ctx["md"] = md.convert(ctx["uporabnik"].description)
    return render(req, 'registration/user.html', context= ctx)

@login_required(login_url='/accounts/login/')
def comment(req, parent):
    if req.method == "POST":
        form = ReplyForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            cnt = data["content"]
            parent = Komentar.objects.get(id=parent)
            uporabnik = req.user.uporabnik
            Komentar.objects.create(parent=parent, content=cnt, user=uporabnik)
            
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))

@login_required(login_url='/accounts/login/')
def report(req, comment_id):
    if req.method == "POST":
        form = ReportForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            comment = get_object_or_404(Komentar, id=comment_id)
            instance = Ticket.objects.create(
                by=req.user.uporabnik,
                information = data["more_information"],
                comment = comment
            )
            instance.report_type.set(data["report_type"])
            instance.save()
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))

# hopefull test if its superuser
def mod_required(
        func=None, 
        alt_url=None, 
        redirect_field_name=REDIRECT_FIELD_NAME
    ):
    inner = user_passes_test(
        lambda u: Superuser.is_mod(u),
        login_url=alt_url,
        redirect_field_name=redirect_field_name
    )
    print(inner)
    if func:
        return inner(func)
    return inner

def no_auth(req):
    return render(req, "mod/no_auth.html") 

@mod_required(alt_url="moderation:no_auth")
def mod_home(req):
    ctx={}
    comments = Komentar.objects.annotate(tickets = Count("ticket")).filter(tickets__gt=0).filter(blocked=False)
    ctx["comments"] = comments
    return render(req, "mod/homepage.html", context=ctx)

@mod_required(alt_url="moderation:no_auth")
def mod_comment(req, comment_id):
    ctx={}
    comment = get_object_or_404(Komentar, id=comment_id)
    ctx["tickets"] = Ticket.objects.filter(comment=comment.id).filter(status="P")
    ctx["comment"] = comment
    return render(req, "mod/comment_page.html", context=ctx)

@mod_required(alt_url="moderation:no_auth")
def user_tickets(req, user_id):
    ctx = {}
    tickets = Ticket.objects.filter(comment_id__user=user_id) #TODO all tickets from this users comments
    ctx["accepted"] = tickets.filter(status="A")
    ctx["pending"] = tickets.filter(status="P")
    ctx["dropped"] = tickets.filter(status="D")
    ctx["canceled"] = tickets.filter(status="C")
    return render(req, "mod/user_tickets.html", context=ctx)



@mod_required(alt_url="moderation:no_auth")
def ticket(req, ticket_id):
    ctx = {}
    ctx["ticket"] = get_object_or_404(Ticket, id=ticket_id)
    print(ctx)
    return render(req, "mod/ticket_page.html", context=ctx)


@mod_required(alt_url="moderation:no_auth")
def block(req, comment_id):
    ctx={}
    comment = get_object_or_404(Komentar, id = comment_id)
    comment.blocked = True
    comment.save()
    tickets = Ticket.objects.filter(comment = comment)
    for ticket in tickets:
        ticket.status = "A"
        ticket.moderator = req.user.uporabnik.superuser
        ticket.save()
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))
