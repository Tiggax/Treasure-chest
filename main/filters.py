import django_filters

from main.models import Uporabnik


class UserFilter(django_filters.FilterSet):
    class Meta:
        model = Uporabnik
        fields = {
            "user__username": ['icontains']
        }