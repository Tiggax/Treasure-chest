from django.apps import AppConfig


class PonudbeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ponudbe'
