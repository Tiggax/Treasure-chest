from django.contrib import admin

from . import models

# Register your models here.

admin.site.register(models.Uporabnik)
admin.site.register(models.Komentar)
admin.site.register(models.Superuser)
admin.site.register(models.ReportType)
admin.site.register(models.Ticket)