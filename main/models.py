from django.db import models
from django.conf import settings
from django.db.models import CharField, Model, IntegerField, ManyToManyField, FloatField, ForeignKey, DateField, BooleanField, OneToOneField, TextField
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


@receiver(post_save, sender=User)
def create_uporabnik(sender,instance,created, *args, **kwargs):
    if created:
        upr = Uporabnik.objects.create(user=instance)
        kom = Komentar.objects.create(user=upr, content="Leave a review!")
        upr.profile_comment = kom
        upr.save()


class Uporabnik(Model):
    user = OneToOneField(User, on_delete=models.CASCADE)
    description = TextField()
    profile_comment = ForeignKey('Komentar', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.user.username}"
    
class Superuser(Model):
    uporabnik = OneToOneField(Uporabnik, on_delete=models.CASCADE)

    def __str__(self):
        return f"S:{self.uporabnik.user.username}"
    
    def is_mod(user):
        return Superuser.objects.filter(uporabnik__user=user).exists()

class Komentar(Model):
    parent = ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)
    date = DateField(auto_now_add=True)
    content = CharField(max_length=200)
    blocked = BooleanField(default=False)
    user = ForeignKey(Uporabnik, on_delete=models.CASCADE)

    def replies(self):
        return Komentar.objects.filter(parent=self)
    

class ReportType(Model):
    category = CharField(max_length=20)
    description = CharField(max_length=200)
    severity = IntegerField()

    def __str__(self):
        return f"{self.category}"

class Ticket(Model):
    STATUS = [
        ('P', 'Pending'),
        ("A", "Accepted"),
        ("C","Canceled"),
        ("D", "Dropped"),
    ]
    by = ForeignKey(Uporabnik, on_delete=models.CASCADE)
    status = CharField(max_length=1, choices=STATUS, default="P")
    report_type = ManyToManyField(ReportType)
    information = TextField()
    moderator = ForeignKey(Superuser, on_delete=models.CASCADE, null=True, blank=True)
    comment = ForeignKey(Komentar, on_delete=models.CASCADE)


    def drop(self):
        self.status = "D"
        self.save()
    def cancel(self):
        self.status = "C"
        self.save()
    def accept(self):
        self.status = "A"
        self.save()