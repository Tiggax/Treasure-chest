"""
URL configuration for treasure_chest project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from . import views


account = ([
    path('profile/', views.profile, name="profile"),
    path('user/', views.search_user, name="search"),
    path('user/<str:username>', views.get_user, name="get"),
], "accounts"
)

mod = (
    [
        path('', views.mod_home, name="homepage"),
        path('ticket/<int:ticket>', views.mod_home, name="ticket"),
        path('no_auth', views.no_auth, name="no_auth"),
        path('user/<int:user>', views.user_tickets, name="user"),
        path('comment/<int:comment_id>', views.mod_comment, name="comment"),
        path('report/<int:comment_id>', views.report, name="report"),
        path('block/<int:comment_id>', views.block, name="block"),
    ], "moderation"
)



#app_name = "main"
urlpatterns = [
    path('admin/', admin.site.urls),
    path("signup/", views.SignUpView.as_view(), name="signup"),
    path("accounts/", include(account)),
    path("accounts/", include("django.contrib.auth.urls")),
    path("", views.index, name="homepage"),
    path('ponudbe/', include("ponudbe.urls")),
    path('moderation/', include(mod)),
    path('comment/<int:parent>', views.comment, name="comment"),
]
