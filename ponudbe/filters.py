import django_filters

from .models import Ponudba, Karta

class PonudbaFilter(django_filters.FilterSet):
    tip = django_filters.ChoiceFilter(choices=Ponudba.TIPI)
    karta = django_filters.ModelChoiceFilter(queryset=Karta.objects.all())
    class Meta:
        model = Ponudba
        fields = {
            "cena": ["lt", "gt"],
            "dodatno": ["icontains"],
        }
    def karta_filter(self, qs, name, value):
        print(f"s: {self}\nqs:{qs}\nname:{name}\nvalue:{value}")
        return qs.filter(**{
            name: value
        })