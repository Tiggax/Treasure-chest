from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from . import views

transactions = ([
    path("buy/<int:ponudba>", views.buy, name="buy"),
    path("confirm/<int:ponudba>", views.confirm, name="confirm"),
    path("recieved/<int:ponudba>", views.recieved, name="recieved"),
    path("finish/<int:ponudba>", views.finish, name="finish"),
    path("cancel/<int:ponudba>", views.cancel, name="cancel"),
    path("suggest/<int:ponudba>", views.suggest, name="suggest"),
], "transactions")

app_name = "ponudbe"
urlpatterns = [
    path("", views.index, name="index"),
    path("get/<int:ponudba_id>", views.get, name="get"),
    path("my", views.moje, name="moje"),
    path("new", views.create, name="new"),
    path("delete/<int:ponudba>", views.delete, name="delete"),
    path("edit/<int:ponudba>", views.edit, name="edit"),
    path("card-autocomplete/", views.KartaAutocomplete.as_view(), name="card_autocomplete"),
    path("transactions/", include(transactions)),
]