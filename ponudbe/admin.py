from django.contrib import admin

from . import models

# Register your models here.

admin.site.register(models.Set)
admin.site.register(models.Barva)
admin.site.register(models.Skupina)
admin.site.register(models.Karta)
admin.site.register(models.Ponudba)
