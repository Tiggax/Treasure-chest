from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect
from django.db.models import Q

from main.forms import ReportForm
from main.models import Komentar
from .forms import EditPonudbaForm, MnenjeForm, PonudbaForm
from django.contrib.auth.decorators import login_required

from dal import autocomplete
from .models import Ponudba, Karta
from .filters import PonudbaFilter

# Create your views here.

class KartaAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        cards = Karta.objects.all()
        if self.q:
            cards = cards.filter(ime__istartswith=self.q) | cards.filter(set_id__tip__istartswith=self.q)
        return cards
    

def index(req):
    ctx = {}
    ponudbe = Ponudba.objects.filter(stanje="N")
    ctx["filter"] = PonudbaFilter(req.GET, queryset=ponudbe)
    print(ctx)
    return render(req, 'ponudbe/ponudbe.html', context=ctx)

def get(req, ponudba_id):
    ctx = {}
    ctx["ponudba"] = get_object_or_404(Ponudba, id=ponudba_id)
    ctx["edit_form"] = EditPonudbaForm(initial={
        "cena": ctx["ponudba"].cena,
        "dodatno": ctx["ponudba"].dodatno,
    })
    ctx["reply_form"] = MnenjeForm()
    ctx["report_form"] = ReportForm()
    return render(req, 'ponudbe/ponudba_page.html', context=ctx)


@login_required(login_url='/accounts/login/')
def create(req):
    ctx = {}
    ctx["cards"] = Karta.objects.all()
    if req.method == "POST":
        form = PonudbaForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            Ponudba.objects.create(
                tip=data["tip"],
                cena=data["cena"],
                dodatno=data["dodatno"],
                karta=data["karta"],
                ustvaril=req.user.uporabnik
                )
            return redirect("ponudbe:moje")
    else:
        form = PonudbaForm()

    ctx["form"] = form
    return render(req, "ponudbe/new_ponudba.html", context=ctx)

@login_required(login_url='/accounts/login/')
def moje(req):
    ctx = {}
    m_ponudbe = Ponudba.objects.filter(ustvaril=req.user.uporabnik)
    d_ponudbe = Ponudba.objects.filter(zakljucil=req.user.uporabnik)
    ctx["na_voljo"] = m_ponudbe.filter(stanje="N")
    ctx["trans"] = m_ponudbe.filter(stanje="T")
    ctx["prevz"] = d_ponudbe.filter(stanje="V")
    ctx["oddaj"] = m_ponudbe.filter(stanje="V")

    ctx["nakup"] = d_ponudbe.filter(stanje="G")
    
    #zakljuceno
    ctx["moji_zaklj"] = m_ponudbe.filter(stanje="Z")
    ctx["drugi_zaklj"] = d_ponudbe.filter(stanje="Z")
    #preklicano
    ctx["moji"] = m_ponudbe.filter(stanje="P")
    ctx["drugi"] = d_ponudbe.filter(stanje="P")

    return render(req, "ponudbe/moje_ponudbe.html", context=ctx)

@login_required(login_url='/accounts/login/')
def delete(req, ponudba):
    ponudba = get_object_or_404(Ponudba, id=ponudba)
    print("delete")
    return redirect("ponudbe:moje")

@login_required(login_url='/accounts/login/')
def edit(req, ponudba):
    ponudba = get_object_or_404(Ponudba, id=ponudba)
    if req.method == "POST":
        form = EditPonudbaForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            print(f"valid: {data}")
            ponudba.dodatno = data["dodatno"]
            ponudba.cena = data["cena"]
            ponudba.save()
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))

@login_required(login_url='/accounts/login/')
def buy(req, ponudba):
    ponudba = get_object_or_404(Ponudba, id=ponudba)
    if ponudba.stanje == "N" and req.user.uporabnik != ponudba.ustvaril:
        ponudba.stanje = "T"
        ponudba.zakljucil = req.user.uporabnik
        ponudba.save()
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))

@login_required(login_url='/accounts/login/')
def confirm(req, ponudba):
    ponudba = get_object_or_404(Ponudba, id=ponudba)
    if ponudba.stanje in ["T","G"]:
        ponudba.stanje = "V"
        ponudba.save()
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))

@login_required(login_url='/accounts/login/')
def recieved(req, ponudba):
    ponudba = get_object_or_404(Ponudba, id=ponudba)
    if ponudba.stanje == "V":
        ponudba.stanje = "Z"
        ponudba.save()
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))


@login_required(login_url='/accounts/login/')
def cancel(req, ponudba):
    ponudba = get_object_or_404(Ponudba, id=ponudba)
    prev = ponudba.stanje
    ponudba.stanje = "C"
    if req.method == "POST":
        form = MnenjeForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            comment = Komentar.objects.create(
                user=req.user.uporabnik,
                content=data["mnenje"],
            )
            ponudba.mnenje = comment
    if prev == "G":
        comm = Komentar.objects.get(content=f"moved to <{ponudba.id}>")
        old = Ponudba.objects.get(mnenje=comm)
        old.stanje = "N"
        old.mnenje = None
        old.save()
    ponudba.save()
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))

@login_required(login_url='/accounts/login/')
def finish(req, ponudba):
    ponudba = get_object_or_404(Ponudba, id=ponudba)
    ponudba.stanje = "Z"
    if req.method == "POST":
        form = MnenjeForm(req.POST)
        if form.is_valid():
            data = form.cleaned_data
            comment = Komentar.objects.create(
                user=req.user.uporabnik,
                content=data["mnenje"],
            )
            ponudba.mnenje = comment
    ponudba.save()
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))


@login_required(login_url='/accounts/login/')
def suggest(req, ponudba):
    ponudba = get_object_or_404(Ponudba, id=ponudba)
    ponudba.stanje = "G"
    new = Ponudba.objects.create(
        stanje = "G",
        tip = "P",
        ustvaril = req.user.uporabnik,
        zakljucil = ponudba.ustvaril,
        dodatno = ponudba.dodatno,
        cena = ponudba.cena,
        karta = ponudba.karta,
    )
    kom = Komentar.objects.create(
        user=req.user.uporabnik,
        content=f"moved to <{new.id}>"
    )
    ponudba.mnenje = kom
    ponudba.save()
    return HttpResponseRedirect(req.META.get('HTTP_REFERER'))
